# MartianRobot
A Coding Challenge App for Red Badger.


## Set up to wok
Install dependencies: `npm install`

### Running the app
Run the server: `nodemon server.js` or `node server.js`
Check the console output for the URL of the app and navigate to it in your browser. 

### Running tests
Go into MartianRobots folder: `karma start``



## Goals  
- Allow one Robot to move through a grid world. 
- Moves can be conducted in all 4 orientations (N, S, E, W).
- Let Robots disappear, when it exceeds the grid dimension.
- Set walls to the grid points, where a Robot has fallen off -to avoid fall offs at this point.
- If another Robot wants to exceed at the same grid point - just ignore this move. 

### Input: 
- First line:  top right grid point -->  Grid dimension (x+1, y+1)
- Remaining: Sequence of robot positions and instructions to move a robot.

### Data Format: 
•	Robot Position: x,y cordinated and orientation (N,S,E,W) – single spaced
•	Robot Instruction:  string of letters: L, R, F (turn left & remain, turn right & remain, move forward 1 grid of the current orientation)

## Output
•	Final position of robot


#  Work Procedure
* 1.	Read through Challenge  - Make notes on 2nd Reading.
* 2.	Draw Grid and move scenarios with the sample data - to better understand the problem in detail.
* 3.	Write Pseudocode create a structure of classes and methods.
* 4.	Set up Angular Project from scratch - to avoid nasty overhead in automatically created projects.
* 5.	Set up Karma.
* 6.	Write Tests for factory methods
* 7.    Write Factory methods.
* 8.    Write Tests for Controller methods.
* 9.    Write Controller methods.



# Next Steps
*       Create FallOfMars Scenario
*       Add Array with Wall ("scent") fields
*       Create algorithms to improve performance of instructions - by eliminating unessecary steps
*       Add User Input for position and instruction
*       Input validation of data
*       End-to-End-Test the solution
*       Animate the world to make it look fancy. 



