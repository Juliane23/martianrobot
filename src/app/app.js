

(function() {
  'use strict';

  angular.module('app', [
    'ui.router',
    'app.robot.controller.base', 
    'app.robot.factory.move'
  ])
  .config(function($urlRouterProvider) {
    $urlRouterProvider.otherwise("/");
  });



})();