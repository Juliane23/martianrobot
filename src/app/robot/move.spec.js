
describe("Robot Base Controller: ", function(){
    var scope, $controller, RobotController, RobotFactoryMove;

    var grid ="53";
    var edgeTopRight=[5,3];

    beforeEach(angular.mock.module('app.robot.controller.base'));  
    beforeEach(angular.mock.module('app.robot.factory.move'));

    beforeEach(inject(function($rootScope, $controller,  _RobotFactoryMove_){
        scope = $rootScope.$new();
        RobotFactoryMove = _RobotFactoryMove_;

        RobotController = $controller('RobotController', {
            '$scope': scope,
        });

    }));


    it('should exist', function(){
        expect(RobotController).toBeDefined();
    });


    
    it("should move Robot to 1 1 E", function(){
        var robot = "1 1 E";
        var instruction = "RFRFRFRF";
        
        scope.testMove();
        expect(scope.output).toEqual("1 1 E");
    })


    it("should move Robot to 3 3 N", function(){
        var robot = "3 2 N";
        var instruction = "FRRFLLFFRRFLL";

        scope.testMove();
        expect(scope.lostOutput).toEqual("3 3 N");
    })

 
});
