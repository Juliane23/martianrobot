

(function (){


    'use strict';

    angular
    .module('app.robot.controller.base', [])
    .controller('RobotController', RobotController);

    RobotController.$inject = [
        '$scope',
        'RobotFactoryMove'
    ];
    
    
    function RobotController($scope, RobotFactoryMove){


         //Test input
         var grid ="53";
         var robot = "3 2 N";
         var instruction = "FRRFLLFFRRFLL";
         var edgeTopRight=[];
         var robotVanished= false;
         var lastPosition;


         //Just integrated to access private function for moveRobot
         $scope.testMove = function(){
             moveRobot();
         };

        function init(){
            $scope.output= " ";
            $scope.isRobotVanished = false;

            createTopRightField();
            moveRobot();

        }

        init();


        function createTopRightField(){
            edgeTopRight= grid.split("").map(function(item){
                return parseInt(item,10);
            });
        }


        function moveRobot(){
            var nextMove;
            

            //1. Take just the robots position from the input
            var robotXYArr = robot.split(" ",2).map(function(item){
                return parseInt(item,10);
            });

            //1b. tage the Orientation from the robot
            var robotOrientation = robot.match(/[NESW]+/g).toString();

            
            //2. analyze the instructions
            for (var i=0; i<instruction.length; i++){
                nextMove=""; //clear variable
                nextMove=instruction.charAt(i);
                console.log("Robot: " + robotXYArr + ", " + robotOrientation);

                if (nextMove === "R"){
                    RobotFactoryMove.turnRight(robotOrientation); 
                    robotOrientation=RobotFactoryMove.getOrientation(); 
                }
                if(nextMove === "L"){
                    RobotFactoryMove.turnLeft(robotOrientation);
                    robotOrientation=RobotFactoryMove.getOrientation();
                }
                
                if(nextMove === "F"){
                    robotXYArr = RobotFactoryMove.moveRobot(robotXYArr, robotOrientation);
                    robotVanished = RobotFactoryMove.robotVanished(robotXYArr, edgeTopRight);
                    if (robotVanished){
                        //Add walls to grid where a former robot was lost
                        RobotFactoryMove.addWall(lastPosition);
                        break;
                    }
                    else{
                        lastPosition = robotXYArr.toString();
                        console.log("lastPostion: " + lastPosition);
                    }
                }
            }
            //3. print the final result
            showResult(robotXYArr, robotOrientation);    
        }

        function showResult(robotXYArr, robotOrientation){
            if(robotVanished){
                $scope.isRobotVanished =true;
                $scope.lostOutput= lastPosition[0] + " " + lastPosition[2] + " " + robotOrientation;
            }
            else{
                $scope.output = robotXYArr[0] + " " + robotXYArr[1] + " " + robotOrientation;
            } 
        }
    }    
    

})();
