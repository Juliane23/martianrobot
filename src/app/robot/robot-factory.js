
(function (){
    
    
        'use strict';
    
        angular
        .module('app.robot.factory.move', [])
        .factory('RobotFactoryMove', RobotFactoryMove);

    function RobotFactoryMove(){

        var factory = {
            orientation: "",
            wall:[]
        }

        factory.getOrientation      = getOrientation;
        factory.setOrientation      = setOrientation;
        factory.turnRight           = turnRight;
        factory.turnLeft            = turnLeft;
        factory.moveRobot           = moveRobot;
        factory.addWall             = addWall;
        factory.robotVanished       = robotVanished;
        


        function turnRight(orientation){
            if(orientation === "N"){
                factory.orientation = "E";
            }
            else if (orientation === "E"){
                factory.orientation = "S";
            }
            else if (orientation === "S"){
                factory.orientation = "W";
            }
            else if (orientation === "W"){
                factory.orientation = "N";
            }
            else console.log("An error occured in RobotFactoryMove.turnRight");
        }


        function turnLeft(orientation){
            if(orientation === "N"){
                factory.orientation = "W";
            }
            else if (orientation === "E"){
                factory.orientation = "N";
            }
            else if (orientation === "S"){
                factory.orientation = "E";
            }
            else if (orientation === "W"){
                factory.orientation = "S";
            }
            else console.log("An error occured in RobotFactoryMove.turnLeft");
        }

        //move robot on step for north the new coordinate is (x, y+1)
        function moveRobot(position, orientation){
    
            //loop through all orientation store new orientation in factory object
            switch (orientation){
                case "N":
                    position[1] +=1;
                    break;
                case "S": 
                    position[1] -=1;
                    break;
                case "E":
                    position[0] +=1;
                    break;
                case "W": 
                    position[0] -=1;
                    break;
                default:
                    console.log("Error in moveRobot - sth wrong with orientation");
            }
           return position;
        }


        function addWall(position){
            factory.wall.push(position);
        }
    
        function robotVanished(position, topRightEdge){
            return (position[0]>topRightEdge[0] || position[1]>topRightEdge[1] ||position[0]<0 || position[1]<0);     
        }



        /*****************
         * GETTER & SETTER
         *****************/

        function getOrientation(){
            return factory.orientation;
        }
        function setOrientation(orientation){
            factory.orientation = orientation;
        }

        function getWall(){
            return factory.wall;
        }
        function setWall(wall){
            factory.wall = wall;
        }



        return factory;

    }

    })();