
describe("Robot factory: ", function(){
    var RobotFactoryMove;
    var position = [1,1];
    var orientation = "E";
    var offGridPosition = [4,3];
    var topRightEdge=[3,5]

    //1. import move factory
    beforeEach(angular.mock.module('app.robot.factory.move'));

    //2. inject function 
    beforeEach(inject(function(_RobotFactoryMove_){
        RobotFactoryMove =  _RobotFactoryMove_;
    }));

    //3. test if factory exists
    it('should exist', function(){
        expect(RobotFactoryMove).toBeDefined();
    });

    //4. Test on all turn and move methods
    describe("turn function", function(){
        it('should turn Robot right from East to South', function() {
            RobotFactoryMove.turnRight("E");
            expect(RobotFactoryMove.getOrientation()).toEqual("S");
          });

          it('should turn Robot left from East to North', function() {
            RobotFactoryMove.turnLeft("E");
            expect(RobotFactoryMove.getOrientation()).toEqual("N");
          });
    });


    describe ("moveRobot function", function(){
        var newPostion; 
        it('should move Robot to east increamenting x by 1', function(){
            newPostion = RobotFactoryMove.moveRobot(position, orientation);
            expect(newPostion).toEqual([2,1]);
        })
    })

    describe("robot vanished", function(){
        it('should return true if out of grid', function(){
            expect(RobotFactoryMove.robotVanished(offGridPosition, topRightEdge)).toBe(true);
        })
    })

});








